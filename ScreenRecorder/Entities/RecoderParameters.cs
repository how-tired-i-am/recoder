﻿using ScreenRecorder.Enums;

namespace ScreenRecorder.Etities
{
    public class RecoderParameters
    {
        public int FramesPerSecond { get; set; }
        public int RecordingTime { get; set; }
        public string FileName { get; set; }
        public string SevenZipArhivePath { get; set; }
        public OutputFileFormats OutputFileFormat { get; set; }
        public int Quality { get; set; }
        public Codecs Codec { get; set; }
    }
}