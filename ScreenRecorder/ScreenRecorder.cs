﻿using ScreenRecorder.Enums;
using ScreenRecorder.Etities;
using ScreenRecorder.Tools;
using SharpAvi;
using SharpAvi.Codecs;
using SharpAvi.Output;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using SevenZip;
using System.IO;

namespace ScreenRecorder
{
    public class ScreenRecorder
    {
        private const string SevenZipLib64Path = "7zipLib\\7z64.dll";
        private const string SevenZipLib32Path = "7zipLib\\7z32.dll";
        private RecoderParameters recoderParameters;
        private int averageFrameCreationTime;
        private Thread screenThread;
        private Rectangle screenSize;
        private ManualResetEvent stopRecordScreen;
        private int delay;
        private AviWriter aviWriter;
        private IAviVideoStream videoStream;
        private RingBuffer screenShotBuffer;
        private byte[] frame;
        private int frameSize;
        private Bitmap frameBitmap;
        private byte[] frame32bit;
        private BitmapData frameBitmapData;
        private SevenZipCompressor sevenZipCompressor;

        public ScreenRecorder(RecoderParameters recoderParameters)
        {
            this.recoderParameters = recoderParameters;
            screenSize = Screen.PrimaryScreen.Bounds;
            aviWriter = CreateAviWriter();
            videoStream = CreateVideoStream(aviWriter);
            sevenZipCompressor = Get7zipCompressor();
            stopRecordScreen = new ManualResetEvent(false);
            delay = 1000 / recoderParameters.FramesPerSecond;
            videoStream.Name = "RobotPlayer";
            frameSize = screenSize.Width * screenSize.Height;
            screenShotBuffer = new RingBuffer(recoderParameters.FramesPerSecond * recoderParameters.RecordingTime, frameSize);
            frame = new byte[frameSize];
            frame32bit = new byte[frameSize * 4];
            frameBitmap = new Bitmap(screenSize.Width, screenSize.Height, PixelFormat.Format8bppIndexed);
            frameBitmapData = new BitmapData();
            
        }

        public void StartRecord()
        {
            screenThread = new Thread(RecordingScreen)
            {
                Name = $"{videoStream.Name}.RecordScreen",
                IsBackground = true
            };
            screenThread.Start();
        }

        private void RecordingScreen()
        {
            delay = 1000 / recoderParameters.FramesPerSecond;
            var processingDelay = 0;
            var frameCreationTime = 0;
            var startTime = DateTime.Now;
            while (!stopRecordScreen.WaitOne(processingDelay))
            {
                startTime = DateTime.Now;
                GetScreenshot();
                frameCreationTime = (DateTime.Now - startTime).Milliseconds;
                processingDelay = delay - frameCreationTime > 0 ? delay - frameCreationTime : 0;
                averageFrameCreationTime = (averageFrameCreationTime + frameCreationTime) / 2;
            }
        }

        private void GetScreenshot()
        {
            var startTime = DateTime.Now;
            using (var scrennShot = new Bitmap(screenSize.Width, screenSize.Height))
            {
                using (var g = Graphics.FromImage(scrennShot))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, new Size(screenSize.Width, screenSize.Height), CopyPixelOperation.SourceCopy);
                    g.Flush();
                    var bits = scrennShot.LockBits(new Rectangle(0, 0, screenSize.Width, screenSize.Height), ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
                    Marshal.Copy(bits.Scan0, frame, 0, frame.Length);
                    screenShotBuffer.AddScreenShot(frame);
                    scrennShot.UnlockBits(bits);
                }
            }
        }

        private void GetVideo()
        {
            var buffer = screenShotBuffer.GetAllFrames();
            for (int i = 0; i < buffer.Count(); i++)
            {
                videoStream.WriteFrame(true, ConvertColor8to32bit(buffer[i]), 0, 1);
            }
        }

        private AviWriter CreateAviWriter()
        {
            return new AviWriter(recoderParameters.FileName)
            {
                FramesPerSecond = recoderParameters.FramesPerSecond,
                EmitIndex1 = true,
            };
        }

        private IAviVideoStream CreateVideoStream(AviWriter writer)
        {
            var codec = CodecIds.Uncompressed;
            switch (recoderParameters.Codec)
            {
                case Codecs.Uncompressed:
                    return writer.AddUncompressedVideoStream(screenSize.Width, screenSize.Height);
                case Codecs.MotionJpeg:
                    return writer.AddMJpegWpfVideoStream(screenSize.Width, screenSize.Height, recoderParameters.Quality);
                case Codecs.Mpeg4:
                    return writer.AddMpeg4VcmVideoStream(
                    screenSize.Width,
                    screenSize.Height,
                    (double)writer.FramesPerSecond,
                    quality: recoderParameters.Quality,
                    codec: codec,
                    forceSingleThreadedAccess: true);
            }
            return null;
        }

        private byte[] ConvertColor8to32bit(byte[] frame8bit)
        {
            frameBitmapData = frameBitmap.LockBits(
                new Rectangle(0, 0, frameBitmap.Width, frameBitmap.Height),
                ImageLockMode.WriteOnly, frameBitmap.PixelFormat);
            Marshal.Copy(frame8bit, 0, frameBitmapData.Scan0, frame8bit.Length);
            frameBitmap.UnlockBits(frameBitmapData);

            var frameClone8to32 = frameBitmap.Clone(new Rectangle(0, 0, screenSize.Width, screenSize.Height), PixelFormat.Format32bppArgb);
            {
                var bits = frameClone8to32.LockBits(new Rectangle(0, 0, screenSize.Width, screenSize.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppPArgb);
                Marshal.Copy(bits.Scan0, frame32bit, 0, frame32bit.Length);
                frameClone8to32.UnlockBits(bits);
            }
            return frame32bit;
        }

        private void SaveVideo()
        {
            aviWriter.Close();
            switch (recoderParameters.OutputFileFormat)
            {
                case OutputFileFormats.Original:
                    break;
                case OutputFileFormats.SevenZip:
                    sevenZipCompressor.CompressFiles(recoderParameters.SevenZipArhivePath, recoderParameters.FileName);
                    if (File.Exists(recoderParameters.FileName))
                    {
                        File.Delete(recoderParameters.FileName);
                    }
                    break;
                case OutputFileFormats.OriginalAndSevenZip:
                    sevenZipCompressor.CompressFiles(recoderParameters.SevenZipArhivePath, recoderParameters.FileName);
                    break;
            }
        }

        public void Dispose()
        {
            stopRecordScreen.Set();
            try
            {
                GetVideo();
                SaveVideo();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("Writing video file error", ex);
            }
            screenThread.Join();
            stopRecordScreen.Dispose();
        }

        public CreationSpeedStatus GetRecoderWorkStatus()
        {
            var StatusWhithPercent = Math.Round((double)(averageFrameCreationTime * 100 / delay));
            if (StatusWhithPercent <= (int)CreationSpeedStatus.Normal)
            {
                return CreationSpeedStatus.Good;
            }
            if (StatusWhithPercent > (int)CreationSpeedStatus.Normal && StatusWhithPercent <= (int)CreationSpeedStatus.Bad)
            {
                return CreationSpeedStatus.Normal;
            }
            if (StatusWhithPercent > (int)CreationSpeedStatus.Bad && StatusWhithPercent < (int)CreationSpeedStatus.Unacceptable)
            {
                return CreationSpeedStatus.Bad;
            }
            if (StatusWhithPercent >= (int)CreationSpeedStatus.Unacceptable)
            {
                return CreationSpeedStatus.Unacceptable;
            }
            return CreationSpeedStatus.Unacceptable;
        }

        private SevenZipCompressor Get7zipCompressor()
        {
            var libPath = Environment.Is64BitProcess ? SevenZipLib64Path : SevenZipLib32Path;
            SevenZipCompressor.SetLibraryPath(libPath);
            var compressor = new SevenZipCompressor();
            compressor.CompressionLevel = CompressionLevel.Ultra;
            return compressor;
        }
    }
}