﻿namespace ScreenRecorder.Enums
{
    public enum Codecs
    {
        Uncompressed = 1,
        MotionJpeg = 2,
        Mpeg4 = 3
    }
}