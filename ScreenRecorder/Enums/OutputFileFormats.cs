﻿namespace ScreenRecorder.Enums
{
    public enum OutputFileFormats
    {
        Original = 0,
        SevenZip = 1,
        OriginalAndSevenZip = 2,
    }
}