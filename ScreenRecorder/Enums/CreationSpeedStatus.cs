﻿using System.ComponentModel;

namespace ScreenRecorder.Enums
{
    [Description("")]
    public enum CreationSpeedStatus
    {
        [Description("Время создания обьекта t <= 40% от задержки между кадарами")]
        Good = 0,
        [Description("Время создания обьекта 40% < t < 80% от задержки между кадарами")]
        Normal = 40,
        [Description("Время создания обьекта 80% <= t < 100% от задержки между кадарами. Возможно стоит снизить FPS")]
        Bad = 80,
        [Description("Время создания обьекта больше задержки между кадарами. Нужно снизить FPS !")]
        Unacceptable = 100
    }
}