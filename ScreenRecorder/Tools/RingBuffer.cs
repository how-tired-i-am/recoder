﻿using ScreenRecorder.Etities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenRecorder.Tools
{
    public class RingBuffer
    {
        private byte [][] buffer;
        private int headPointer;
        private int frameCount;

        public RingBuffer(int frameCount, int frameSize)
        {
            this.frameCount = frameCount;
            buffer = new byte[frameCount][];
            BufferInitialization(frameSize);
        }

        private void BufferInitialization(int frameSize)
        {
            for (int i = 0; i < buffer.Count(); i++)
            {
                buffer[i] = new byte[frameSize];
            }
        }

        public void AddScreenShot(byte[] frame)
        {
            if (headPointer != frameCount - 1)
            {
                headPointer++;
            }
            else
            {
                headPointer = 0;
            }
            Array.Copy(frame, buffer[headPointer], frame.Length);
        }

        public byte[][] GetAllFrames()
        {
           var result = new byte[frameCount][];
           Array.Copy(buffer, headPointer + 1, result, 0, frameCount - headPointer - 1);
           Array.Copy(buffer, 0, result, frameCount - headPointer - 1, headPointer + 1);
           return result;
         }
    }
}
