﻿using ScreenRecorder.Etities;
using ScreenRecorder.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenRecorder
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var rec = new ScreenRecorder(new RecoderParameters 
            { 
                RecordingTime = 10, 
                FramesPerSecond = 8, 
                FileName = "D:\\rec.avi", 
                Quality = 30, 
                Codec = Enums.Codecs.MotionJpeg,
                SevenZipArhivePath = "D:\\rec.7z",
                OutputFileFormat = Enums.OutputFileFormats.SevenZip
            });
            rec.StartRecord();
            Console.WriteLine($"Идет запись экрана, для сохранения видео нажмите любую клавишу");
            Console.ReadKey();
            rec.Dispose();
            Console.WriteLine($"Тест производительности рекодера: {rec.GetRecoderWorkStatus()}");
            Console.WriteLine($"Нажмите клавишу для выхода...");
            Console.ReadKey();
        }
    }
}
